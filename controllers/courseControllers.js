const Course = require("../models/Course");
const auth = require("../auth");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/
// module.exports.addCourse = (reqBody) => {

// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 	});

// 	return newCourse.save().then((course, error) => {

// 		if (error) {

// 			return false;

// 		} else {

// 			return true;

// 		};

// 	});

// };

module.exports.addCourse = (data) => {

	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {

			// Course creation successful
			if (error) {

				return false;

			// Course creation failed
			} else {

				return true;

			};

		});

	// User is not an admin
	} else {
		return false;
	};
	

};

// Retrieve all courses
/*
	Steps:
	1. Retrieve all the courses from the database
*/
module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {

		return result;

	});

};

// Retrieve all ACTIVE courses
/*
	Steps:
	1. Retrieve all the courses from the database with the property of "isActive" to true
*/
module.exports.getAllActive = () => {

	return Course.find({isActive : true}).then(result => {
		return result;
	});

};

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});

};

// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// Information to update a course will be coming from both the URL parameters and the request body
module.exports.updateCourse = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let updatedCourse = {
		name : reqBody.name,
		description	: reqBody.description,
		price : reqBody.price
	};

	// Syntax
		// findByIdAndUpdate(document ID, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

		// Course not updated
		if (error) {

			return false;

		// Course updated successfully
		} else {
			
			return true;
		};

	});

};