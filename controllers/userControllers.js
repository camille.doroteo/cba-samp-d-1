const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Check if the email already exists
/*
	Steps: 
			1. Use mongoose "find" method to find duplicate emails
			2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result=>{
		if(result.length>0){
			return true;
		}else{
			return false;
		}
	})
}

//User registration
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
};

//User Authentication

module.exports.loginUser = (reqBody) =>{
	return User.findOne({email: reqBody.email}).then(result=>{
		if(result==null){
			return false;
		}else{

		const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
		
		if(isPasswordCorrect){
			return {access: auth.createAccessToken(result)}
		}else{
			return false;
		}
	   }
	})
}

//Retrieve user details
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Reassign the password of the returned document to an empty string
	3. Return the result back to the frontend
*/

// module.exports.getProfile = (reqBody) =>{
// 	return User.findById(reqBody.id).then(result=>{
// 			result.password = "";	
// 			return result;
// 	});
// };

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;

	});

};

// Enroll user to a class
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/
// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enroll = async (data) => {

	// Add the course ID in the enrollments array of the user
	// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend
	let isUserUpdated = await User.findById(data.userId).then(user => {

		// Adds the courseId in the user's enrollments array
		user.enrollments.push({courseId : data.courseId});

		// Saves the updated user information in the database
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});

	// Add the user ID in the enrollees array of the course
	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : data.userId});

		// Saves the updated course information in the database
		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});

	// Condition that will check if the user and course documents have been updated
	// User enrollment successful
	if(isUserUpdated && isCourseUpdated){
		return true;
	// User enrollment failure
	} else {
		return false;
	};

};