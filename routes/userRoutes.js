const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})

router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// //Route for retrieving user details
// router.post("/details",(req,res)=>{
// 	userController.getProfile(req.body).then(resultFromController=>res.send(resultFromController));
// });

router.get("/details",auth.verify,(req,res)=>{
	
	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
});


// Route to enroll user to a course
router.post("/enroll", (req, res) => {

	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));

})

module.exports = router;